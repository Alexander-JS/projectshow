document.addEventListener( 'DOMContentLoaded', ()=> {

import removeR from './removeR';
removeR();


document.addEventListener( 'keydown', ( e )=>{
	let key = e.key;
	if( key === 'c' ){
		localStorage.clear();
	}
});
let cl = ( item )=>{ console.log( item ); }
class Place {
	constructor( tr, td ){
		this.row = tr;
		this.place = td;
		this.busy = false;
		this.show = "ряд "+tr+", место "+td+" - <span class='money'>115</span> грн";
		// this.sale = false;
	}
}
let ticket = document.getElementById( 'ticket' );
let li__total__coast = document.getElementById( 'li__total__coast' );
	let span = li__total__coast.querySelector( 'span' );
let total__coast = 0;
let loats = [];
let add__to__basket = ( obj ) => {
	total__coast+= 115;
	loats.push( [obj.row, obj.place] );
	let li = document.createElement( 'li' );
	li.innerHTML = obj.show;
	let clear__ul = () => {
		total__coast -= 115;
		li.classList.add( 'remove' );
		setTimeout( ()=>{
			ticket.removeChild( li );	
		}, 1000 );
		
		span.innerHTML = total__coast;
		clear__me( null, obj );
	}
	li.addEventListener( 'click', clear__ul );
		setTimeout( clear__ul, 60000 );
	ticket.insertBefore( li, li__total__coast );
	span.innerHTML = total__coast;
}
let clear__me = ( td, obj ) => {
	if( td !== null ){
		td.classList.add( 'unclick' );		
	}
	obj.busy = false;
	render__hall( hall_arr.length, hall_arr[0].length );
	save( hall_arr );	
	for( let i = 0; i < loats.length; i++ ){
		for( let j = 0; j < loats[i].length; j++ ){
			if( obj.row === loats[i][j] ){
				if( obj.place === loats[i][j+1] ){
					return i;
				}
			}	
		}
		
	}
}
let json_hall_arr = localStorage.getItem( 'hall_arr' );
let hall_arr = [];
console.log( hall_arr );
let unsaved = false;

if( json_hall_arr !== null && json_hall_arr !== undefined ){
	hall_arr = JSON.parse( json_hall_arr );
} else {
	unsaved = true;
}
let save = ( item ) => {
	let test = JSON.stringify( item );
	localStorage.setItem( 'hall_arr', test );
}

let section = document.getElementById( 'hall' );
let tableS = section.querySelector( '._hall__tabble' );

function render__hall( tr, td ) {
	section.innerHTML = '<div class="screen">ЄКРАН</div>';
		if( unsaved ){
			for( let i = 0; i < tr; i++ ){
				hall_arr[ i ] = [];

				for( let j = 0; j < td; j++ ){
					hall_arr[i][j] = new Place( (i+1), (j+1) );
				}
			}
		}
		let table = document.createElement( 'table' );
			table.classList.add( '_hall__tabble' );
		for( let i = 0; i < tr; i++ ){
			let tr = document.createElement( 'tr' );
		for( let j = 0; j < td; j++ ){
			let td = document.createElement( 'td' );
				if( hall_arr[i][j].busy ){
					td.classList.add( 'clicked' );	
				} else {
					td.classList.add( 'unclick' );
				}
				td.addEventListener( 'click', ()=>{
					if( hall_arr[i][j].busy === false ){
						add__to__basket( hall_arr[i][j] );
					}
					td.classList.add( 'clicked' );
					hall_arr[i][j].busy = true;
					setTimeout( ()=>{
						let li_for_delete = clear__me( td, hall_arr[i][j] );
					},60000);
					save( hall_arr );
				});
				tr.appendChild( td );
				table.appendChild( tr );
			}
		}
		section.appendChild( table );
	}
	render__hall( 8, 16 );
});