/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/


const _government = {
  laws: [{

      id: 0,
      text: '123123'
  }],

  budget: 1000000,
  citizensSatisfactions: 0,
};

const methods = {
  // code:


  addLaw: function ( id, name, description ) {
    let newLw = {
      id,
      name,
      description
    }
    _government.laws.push( newLw );
  },

  readConstID: ( id  ) => {

    console.log( 
      `
      id: ${ id }
      name: ${ _government.laws[ id-1 ].id }
      description: ${ _government.laws[ id-1 ].text }
      `)
  },

  readConstAll: () => {
    let i = -1;
    _government.laws.forEach( ( item ) => {
      console.log( `

      id: ${ item[ i ] }, 

      name: ${ _government.laws[ i ].name }

      description:
      ${ _government.laws[ i ].description }\n\n\n` )
      i++;
    });
  }
}



export default Object.freeze( methods );