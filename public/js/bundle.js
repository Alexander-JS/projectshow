/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/imports.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/imports.js":
/*!********************************!*\
  !*** ./application/imports.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_singleton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/singleton */ \"./classworks/singleton.js\");\n  /*\n    import defaultExport from \"module-name\"; <- node_modules\n    import * as name from \"module-name\";\n    import { export } from \"module-name\";\n    import { export as alias } from \"module-name\";\n    import { export1 , export2 } from \"module-name\";\n    import { export1 , export2 as alias2 } from \"module-name\";\n    import defaultExport, * as name from \"module-name\";\n    import \"module-name\";\n  */\n  /*\n    import defaultExport from \"module-name\";\n  */\n\n// import Singlton from './singleton';\n\n// import objectFreeze from '../classworks/objectfreeze';\n\n\n\n// objectFreeze();\n\n\n_classworks_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].addLaw( 1, 'testName', 'testTxt' );\n_classworks_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].readConstID( 1 );\n\n//# sourceURL=webpack:///./application/imports.js?");

/***/ }),

/***/ "./classworks/singleton.js":
/*!*********************************!*\
  !*** ./classworks/singleton.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:\n\n    Написать синглтон, который будет создавать обьект government\n\n    Данные:\n    {\n        laws: [\n        {\n          id: 0,\n          text: '123123'\n        }\n      ],\n        budget: 1000000\n        citizensSatisfactions: 0,\n    }\n\n    У этого обьекта будут методы:\n      .добавитьЗакон({id, name, description})\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\n\n      .читатькКонституцию -> Вывести все законы на экран\n      .читатьЗакон(ид)\n\n      .показатьУровеньДовольства()\n      .показатьБюджет()\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\n\n\n*/\n\n\nconst government = {\n  laws: [{\n\n      id: 0,\n      text: '123123'\n  }],\n\n  budget: 1000000,\n  citizensSatisfactions: 0,\n};\n\nconst methods = {\n  // code:\n\n\n  addLaw: function ( { id, name, description } ) {\n    let newLw = {\n      id: id,\n      name: name,\n      description: description\n    }\n    government.laws.push( newLw );\n  },\n\n  readConstID: ( id  ) => {\n\n    console.log( \n      `\n      id: ${ id }\n      name: ${ government.laws[ id ].id }\n      description: ${ government.laws[ id ].text }\n      `)\n  },\n\n  readConstAll: () => {\n    let i = -1;\n    government.laws.forEach( ( item ) => {\n      console.log( `\n\n      id: ${ item[ i ] }, \n\n      name: ${ government.laws[ i ].name }\n\n      description:\n      ${ governments.laws[ i ].description }\\n\\n\\n` )\n      i++;\n    });\n  }\n}\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object.freeze( methods ));\n\n//# sourceURL=webpack:///./classworks/singleton.js?");

/***/ })

/******/ });